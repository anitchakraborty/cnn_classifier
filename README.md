# Image Classification

Image Classification CNN model to classify Cat-Dog objects.

## Dataset
- The model is trained on [kaggle Dogs vs. Cats Dataset](https://www.kaggle.com/c/dogs-vs-cats/)
- Download the Dataset with [Download All](https://www.kaggle.com/c/dogs-vs-cats/data) and extract the zip in the same directory. Then one can proceed with the notebook.

## Requirements
- tensorflow
- keras
- numpy

These requirements can be easily installed by:
  `pip install -r requirements.txt`

